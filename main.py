from fastapi import FastAPI

app = FastAPI()

@app.get('/luna')
def luna():
  return {
    'request': 'get',
    'what': 'luna'
  }
